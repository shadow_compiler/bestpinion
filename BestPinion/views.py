from django.shortcuts import render
from .forms import Opinion


def home(request):
    if request.method == 'POST':
        form = Opinion(request.POST)
        if form.is_valid():
            form.save()
    else:
        form = Opinion()
    return render(request, 'home.html', {'form':form})
