from django.shortcuts import render, redirect
from . forms import user_sign 

# Create your views here.
def signup(request):
    if request.method == 'POST':
        form = user_sign(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            return redirect('Home')

    else:
        form = user_sign()
    return render(request, 'accounts/signup.html', {'form':form})


